package com.example.oauth2;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class OauthApplication{
	public static void main(String[] args) {
		SpringApplication.run(OauthApplication.class, args);
	}
}